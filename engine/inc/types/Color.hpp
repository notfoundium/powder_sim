#pragma once

#ifndef COLOR_CLASS_HPP
# define COLOR_CLASS_HPP

class Color
{
public:
	using color_t = unsigned int;

	static const color_t WHITE = 0xffffffff;
	static const color_t YELLOW = 0xffff00ff;
	static const color_t BLUE = 0x0000ffff;

private:
	unsigned int hex;

public:
	Color() : hex(0xffffff) {}

	Color(unsigned int hex) : hex(hex) {}

	Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
	: hex(assemble(r, g, b, a))
	{}

	~Color();

	static unsigned int assemble(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
	{
		return (a << 24) | (b << 16) | (g << 8) | r;
	}

};

#endif /* COLOR_CLASS_HPP */
