#pragma once

#ifndef VECTOR_CLASS_HPP
# define VECTOR_CLASS_HPP

class Vector
{
public:
	int x;
	int y;

public:
	Vector() : x(0), y(0) {}
	Vector(int x, int y) : x(x), y(y) {}
};


#endif /* VECTOR_CLASS_HPP */
