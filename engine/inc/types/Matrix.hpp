#pragma once

#ifndef MATRIX_CLASS_HPP
# define MATRIX_CLASS_HPP

# include <vector>

template <class Container>
class Array2DWrapper {
	private:
		class DimensionProxy {
			public:
				DimensionProxy(Container& container, size_t offset) : container(container), offset(offset) {}

				auto& operator[](size_t index) const { return container[offset + index]; }

			private:
				Container&	container;
				size_t			offset;
	};

	public:

		Array2DWrapper(Container& container, size_t dim_size) : container(container), dim_size(dim_size) {}

		Array2DWrapper& operator=(Array2DWrapper const& array) {
			dim_size = array.dim_size;
			container = array.container;
			return *this;
		}

		DimensionProxy operator[](size_t index) const { return DimensionProxy(container, dim_size * index); }

	private:
		Container&	container;
		size_t		dim_size;
};

template<class T>
using Matrix = Array2DWrapper<std::vector<T>>;

#endif /* MATRIX_CLASS_HPP */
