#pragma once

#ifndef TEXTURE_CLASS_HPP
# define TEXTURE_CLASS_HPP

# include <vector>
# include <iostream>

# include "Color.hpp"
# include "Matrix.hpp"

class Texture
{
public:
	using color_t = Color::color_t;

// Temporary public
public:
	Vector	dim;

	std::vector<color_t>	data;

public:
	Texture(std::size_t width, std::size_t height)
	: dim(width, height)
	, data(std::vector<color_t>(width * height, 0xffffffff))
	{}

	Texture(std::vector<color_t> data, std::size_t width, std::size_t height)
	: dim(width, height)
	, data(data)
	{}

	~Texture() {}

	Texture& operator=(Texture const& texture) {
		dim = texture.dim;
		data = texture.data;
		return *this;
	}

	color_t&	operator[](std::size_t index) { return data[index]; }

	void set_pixel(std::size_t x, std::size_t y, color_t color) {
		if (x >= 0 && x < dim.x && y >= 0 && y <= dim.y) {
			// int idx = (dim.y - 1 - y) * dim.x + x;
			data[y * dim.x + x] = color;
		}
	}

	void print() const
	{
		std::cout << "Dimensions: " << dim.x << "x" << dim.y << std::endl;
		std::cout << std::hex;
		for (int y = 0; y < dim.y; ++y)
		{
			std::cout << std::endl;
			for (int x = 0; x < dim.x; ++x)
				std::cout << data[y * dim.x + x] << " ";
			std::cout << y;
		}
	}

	Vector const& get_dimensions() const { return dim; }
	std::vector<color_t> const& get_data() const { return data; }
};

#endif /* TEXTURE_CLASS_HPP */
