#pragma once

#ifndef ENGINE_CLASS_HPP
# define ENGINE_CLASS_HPP

# include "Window.hpp"
# include "Mouse.hpp"
# include "Object.hpp"

class Engine
{
	public:
		static Mouse mouse;

		static Window window;

	private:
		std::vector<Object*> objects;

	public:
		Engine(/* args */);
		~Engine();

		void loop();
		void add_object(Object* object);
		void render(Object const& object);

		static void error_callback(int error, const char *description);
		static void mouse_button_callback(GLFWwindow *window, int button, int action, int mods);
		static void cursor_position_callback(GLFWwindow *window, double x, double y);
		// static void glDebugMessageCallback(DEBUGPROC callback​, void* userParam​);

	private:
		void init_callbacks();
};


#endif /* ENGINE_CLASS_HPP */
