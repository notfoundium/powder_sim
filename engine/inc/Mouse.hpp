#pragma once

#ifndef MOUSE_CLASS_HPP
# define MOUSE_CLASS_HPP

# include "Device.hpp"

class Mouse : public Device
{
	public:
		double	x;
		double	y;
		int		lmb_state;
		int		rmb_state;

	public:
		Mouse() {}
		~Mouse() {}
};

#endif /* MOUSE_CLASS_HPP */
