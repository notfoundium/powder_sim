#pragma once

#ifndef OBJECT_CLASS_HPP
# define OBJECT_CLASS_HPP

# include "Vector.hpp"
# include "Texture.hpp"

class Object
{
protected:
	Vector		position;
	Texture*	texture = nullptr;

public:
	virtual ~Object() {};

	bool has_texture() const { return texture != nullptr; }

	virtual void update(int delta) = 0;

	inline Texture const&	get_texture() const	{ return *texture; }
	inline Vector const&	get_pos() const		{ return position; }

	void	set_texture(Texture& texture)	{ this->texture = &texture; }
	void	set_pos(Vector const& pos)		{ this->position = pos; }
};

#endif /* OBJECT_CLASS_HPP */
