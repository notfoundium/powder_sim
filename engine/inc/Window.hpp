#pragma once

#ifndef WINDOW_CLASS_HPP
# define WINDOW_CLASS_HPP
# define GLFW_INCLUDE_GLU
# define GL_EN

# include <cstdlib>
# include <string>

# include "GLFW/glfw3.h"

# include "Color.hpp"
# include "Vector.hpp"
# include "Matrix.hpp"

# define PIXEL_SIZE 8

class Window {
	public:
		using pixel_t = Color::color_t;

	private:
		std::size_t	width;
		std::size_t	height;
		GLFWwindow*	ptr;

		std::vector<pixel_t>	screen_data;

	public:
		Window(std::size_t w, std::size_t h, std::string const& name);
		Window(Window const&) = default;
		Window(Window&&) = default;
		~Window();

		Window& operator=(Window const& window) = default;
		Window& operator=(Window&& windoe) = default;

		inline GLFWwindow*	get_ptr()	{ return ptr; }

		inline std::size_t	get_width() const	{ return width; }
		inline std::size_t	get_height() const	{ return height; }

		void	draw_pixel(pixel_t pixel, Vector const& pos);
		void	draw_texture(std::vector<pixel_t> const& data, Vector const& size, Vector const& pos);

		void	update();

};

#endif /* WINDOW_CLASS_HPP */
