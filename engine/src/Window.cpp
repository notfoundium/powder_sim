#include "Window.hpp"

#include <iostream>

Window::Window(std::size_t width, std::size_t height, std::string const& name)
: width(width)
, height(height)
, screen_data(width * height, Color::assemble(255, 255, 255, 255))
{
	if (!glfwInit())
		printf("%s\n", "Error");
	ptr = glfwCreateWindow(width, height, name.c_str(), NULL, NULL);
	// glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwMakeContextCurrent(ptr);
	glPixelZoom(PIXEL_SIZE, PIXEL_SIZE);
}

Window::~Window()
{
	glfwDestroyWindow(ptr);
}

void	Window::draw_pixel(pixel_t pixel, Vector const& pos)
{
	int width = 0;
	int height = 0;

	glfwGetWindowSize(ptr, &width, &height);

	if (pos.x >= 0 && pos.x < width && pos.y >= 0 && pos.y < height)
		screen_data[pos.y * width + pos.x] = pixel;
}

void	Window::draw_texture(std::vector<pixel_t> const& data, Vector const& size, Vector const& pos)
{
	for (std::size_t y = 0; y < size.y; ++y) {
		for (std::size_t x = 0; x < size.x; ++x) {
			draw_pixel(data[y * size.x + x], Vector(x + pos.x, y + pos.y));
		}
	}
}

void Window::update()
{
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glRasterPos2f(-1, -1);

	glDrawPixels(width, height, GL_RGBA, GL_UNSIGNED_BYTE, screen_data.data());
	glfwSwapBuffers(ptr);
}
