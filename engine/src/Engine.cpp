#include "Engine.hpp"

#include <iostream>

Mouse Engine::mouse;
Window Engine::window = Window(640, 480, "Powder sim");

Engine::Engine()
{
	init_callbacks();
}

Engine::~Engine() {
	// glfwTerminate();
}

void Engine::error_callback(int error, const char *description) {
	fprintf(stderr, "Error: %s\n", description);
}

void Engine::mouse_button_callback(GLFWwindow *window, int button, int action, int mods) {
	return ;
}

void Engine::cursor_position_callback(GLFWwindow *window, double x, double y) {
	glfwGetCursorPos(window, &mouse.x, &mouse.y);
}

void Engine::init_callbacks() {
	glfwSetErrorCallback(&Engine::error_callback);
	glfwSetCursorPosCallback(window.get_ptr(), Engine::cursor_position_callback);
}

void Engine::loop()
{
	while (!glfwWindowShouldClose(window.get_ptr()))
	{
		for (Object* object : objects)
		{
			object->update(0);
			render(*object);
		}
		mouse.lmb_state = glfwGetMouseButton(window.get_ptr(), GLFW_MOUSE_BUTTON_LEFT);
		mouse.rmb_state = glfwGetMouseButton(window.get_ptr(), GLFW_MOUSE_BUTTON_RIGHT);
		window.update();
		glfwPollEvents();
	}
}

void Engine::add_object(Object* object)
{
	objects.push_back(object);
}

void Engine::render(Object const& object)
{
	if (object.has_texture()) {
		Texture const& texture = object.get_texture();
		window.draw_texture(texture.data, texture.get_dimensions(), object.get_pos());
	}
}
