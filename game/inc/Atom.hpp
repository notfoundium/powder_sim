#pragma once

#ifndef ATOM_CLASS_HPP
# define ATOM_CLASS_HPP

# include "Color.hpp"

enum class AtomType : unsigned char {
	AIR,
	BEDROCK,
	SAND,
	WATER
};

class Atom {
	public:
		static const AtomType		type = AtomType::AIR;
		static const Color::color_t	color = Color::WHITE;

	public:
		void		expose() const;
};

class AtomSand : protected Atom {
	public:
		static const AtomType		type = AtomType::AIR;
		static const Color::color_t	color = Color::YELLOW;
};

class AtomWater : protected Atom {
	public:
		static const AtomType		type = AtomType::AIR;
		static const Color::color_t	color = Color::BLUE;
};

#endif /* ATOM_CLASS_HPP */
