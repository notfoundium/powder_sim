#pragma once

#ifndef WORLD_CLASS_HPP
# define WORLD_CLASS_HPP

# include <cstdlib>

# include "Engine.hpp"

# include "Object.hpp"
# include "Vector.hpp"
# include "Matrix.hpp"
# include "Atom.hpp"

class World : public Object
{
private:
	Vector dim;

	std::vector<AtomType>	data;

public:
	World(std::size_t size_x, std::size_t size_y);

	~World();

	bool set_atom(AtomType atom, Vector const& pos);

	// AtomType get_atom(std::size_t x, std::size_t y) const;
	AtomType get_atom(Vector const& pos) const;

	bool atom_is_grounded(Vector const& pos) const;
	bool atom_can_flow(Vector const& pos) const;

	void update(int delta) override;

	void process();
	void process_solid(Vector const& pos);
	void process_liquid(Vector const& pos);

	void print() const;

};


#endif /* WORLD_CLASS_HPP */
