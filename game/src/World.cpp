#include "World.hpp"

#include <iostream>
#include <cstdlib>
#include <cmath>

World::World(std::size_t size_x, std::size_t size_y)
	: dim(size_x, size_y)
	, data(size_x * size_y, AtomType::AIR)
{
	texture = new Texture(size_x, size_y);
	for (int y = 0; y < size_y; ++y)
		for (int x = 0; x < size_x; ++x)
			if ((x == 0 || x == size_x - 1) || (y == 0 || y == size_y - 1))
				set_atom(AtomType::BEDROCK, {x, y});
}

World::~World()
{
	delete texture;
}


bool World::atom_is_grounded(Vector const& pos) const
{
	if (get_atom({pos.x, pos.y - 1}) == AtomType::AIR
		|| get_atom({pos.x, pos.y - 1}) == AtomType::WATER)
		return false;
	return true;
}

bool World::atom_can_flow(Vector const& pos) const
{
	if (get_atom({pos.x, pos.y - 1}) == AtomType::AIR)
		return true;
	return true;
}


// AtomType World::get_atom(std::size_t x, std::size_t y) const
// {
// 	return data[y * dim.x + x];
// }

AtomType World::get_atom(Vector const& pos) const
{
	return data[pos.y * dim.x + pos.x];
}

void World::process_solid(Vector const& pos)
{
	if (get_atom(pos) == AtomType::SAND)
	{
		if (!atom_is_grounded(pos))
		{
			AtomType buf_atom = get_atom({pos.x, pos.y - 1});
			set_atom(get_atom(pos), {pos.x, pos.y - 1});
			set_atom(buf_atom, pos);
		}
	}
}

void World::process_liquid(Vector const& pos)
{
	if (get_atom(pos) == AtomType::WATER)
	{
		int direction = std::rand() % 3 - 1;
		if (!atom_is_grounded(pos))
		{
			AtomType buf_atom = get_atom({pos.x, pos.y - 1});
			set_atom(get_atom(pos), {pos.x, pos.y - 1});
			set_atom(buf_atom, pos);
		}
		if (get_atom({pos.x + direction, pos.y}) == AtomType::AIR)
		{
			set_atom(get_atom(pos), {pos.x + direction, pos.y});
			set_atom(AtomType::AIR, pos);
		}
	}
}

void World::process()
{
	for (int y = 0; y < dim.y; ++y)
	{
		for (int x = 0; x < dim.x; ++x)
		{
			AtomType atom = get_atom({x, y});
			if (atom != AtomType::AIR) {
				process_solid({x, y});
				process_liquid({x, y});
			}
		}
	}
}

void World::update(int delta)
{
	if (Engine::mouse.lmb_state == GLFW_PRESS)
	{
		int sel_x = std::round(Engine::mouse.x);
		int sel_y = std::round(Engine::mouse.y);
		set_atom(AtomType::SAND, {sel_x / PIXEL_SIZE, ((int)Engine::window.get_height() - sel_y) / PIXEL_SIZE});
	}
	if (Engine::mouse.rmb_state == GLFW_PRESS)
	{
		int sel_x = std::round(Engine::mouse.x);
		int sel_y = std::round(Engine::mouse.y);
		set_atom(AtomType::WATER, {sel_x / PIXEL_SIZE, ((int)Engine::window.get_height() - sel_y) / PIXEL_SIZE});
	}
	process();
}

bool World::set_atom(AtomType atom, Vector const& pos)
{
	int offset = std::rand() % 20 - 10;

	data[pos.y * dim.x + pos.x] = atom;
	if (atom == AtomType::AIR)
		texture->set_pixel(pos.x, pos.y, 0xffffffff);
	else if (atom == AtomType::BEDROCK)
		texture->set_pixel(pos.x, pos.y, 0xff000000);
	else if (atom == AtomType::SAND)
		texture->set_pixel(pos.x, pos.y, Color::assemble(194 + offset, 178 + offset, 128 + offset, 255));
	else if (atom == AtomType::WATER)
		texture->set_pixel(pos.x, pos.y, Color::assemble(12 + offset, 75 + offset, 134 + offset, 255));
	return true;
}

void World::print() const
{
	for (int y = 0; y < dim.y; ++y)
	{
		std::cout << std::endl;
		for (int x = 0; x < dim.x; ++x)
			std::cout << (int)get_atom({x, y}) << " ";
	}
	std::cout << std::endl;
	texture->print();
}
