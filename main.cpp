#include <stdio.h>

#include "Engine.hpp"
#include "World.hpp"
#include <iostream>

void APIENTRY funcname(GLenum source, GLenum type, GLuint id,
   GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
	std::cout << message << std::endl;
   }

int main(int argc, char* argv[])
{
	glEnable(GL_DEBUG_OUTPUT);
	Engine engine;
	World world(50, 50);
	engine.add_object(&world);
	// world.print();


	engine.loop();
	return 0;
}
