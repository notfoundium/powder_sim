NAME = powder_sim
OUTDIR = ./bin
OUTFILE = $(OUTDIR)/$(NAME)

CXX = clang++
CXXFLAGS = -O3
CXXSTD =

EXT_INC = -I/usr/include/GLFW

INC =	-Iengine/inc \
		-Iengine/inc/types \
		-Igame/inc

SRC =	main.cpp \
		engine/src/*.cpp \
		game/src/*.cpp

LIB =	-lglfw \
		-lGL

all:
	$(CXX) $(CXXFLAGS) $(CXXSTD) $(SRC) $(INC) $(EXT_INC) $(LIB) -o $(OUTFILE)
